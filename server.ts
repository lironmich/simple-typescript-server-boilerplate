import * as express from 'express';
import * as bodyParser from 'body-parser';
import { Logger } from './public/logger/logger';
import { HomeRoute } from './routes/homeRoute';
import { IRoute } from './routes/Route';

process.chdir(__dirname);

export const app = express();
const router = express.Router();
const logger = new Logger();
const port = process.env.port || 8080;

app.use(router);
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: false }));

const routes: IRoute[] = [new HomeRoute(router)];
routes.forEach(route => route.register());

app.listen(port, () => {
    logger.info(`Express server listening or port ${port}`);
});