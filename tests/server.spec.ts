import * as request from 'supertest';
import { app } from '../server';

describe('Server', () => {
  describe('HomeRoute', () => {
    it('/home/index should be correct', (done) => {
      request(app)
        .get('/home/index')
        .expect('Welcome!', done);
    });
  });
});
