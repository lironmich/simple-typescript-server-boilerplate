import * as winston from 'winston';

export interface ILogger {
    info(message: string): void;
    warning(message: string): void;
    error(message: string): void;
}

export class Logger implements ILogger {
    private logger: winston.Logger;

    constructor() {
        this.logger = winston.createLogger({
            transports: [
                new winston.transports.Console()
            ]
        });
    }

    public info(message: string): void {
        this.executeLog(this.logger.info, message);
    }

    public warning(message: string): void {
        this.executeLog(this.logger.warning, message);
    }

    public error(message: string): void {
        this.executeLog(this.logger.error, message);
    }

    private executeLog(logMethod: winston.LeveledLogMethod, message: string) {
        logMethod(message);
    }
}