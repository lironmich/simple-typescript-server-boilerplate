import { Route } from "./route";

export class HomeRoute extends Route {
    public register(): void {
        this.registerIndexAction();
    }

    private registerIndexAction(): void {
        this.router.get("/home/index", (req, res) => {
            res.send("Welcome!");
        })
    }
}