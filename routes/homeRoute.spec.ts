import * as Sinon from 'sinon';
import { expect } from 'chai';
import { HomeRoute } from './homeRoute';
import { IRoute } from './route';
import { Router } from 'express';

describe('HomeRoute', () => {
    let homeRoute: IRoute;
    let routerGetStub: Sinon.SinonStub;

    beforeEach(() => {
        const router: Router = <any>{ get: () => { } };

        routerGetStub = Sinon.stub(router, 'get');

        homeRoute = new HomeRoute(router);
    });

    describe('register', () => {
        beforeEach(() => {
            homeRoute.register();
        });

        it('should register all routes', (done) => {
            expect(routerGetStub.calledOnce).to.be.true;
            expect(routerGetStub.args[0][0]).to.be.equals('/home/index');
            done()
        });
    });
});