import { Router } from "express";

export interface IRoute {
    register(): void;
}
export abstract class Route implements IRoute {
    constructor(protected router: Router) {
    }

    abstract register(): void;
}